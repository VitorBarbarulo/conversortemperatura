import javax.swing.JOptionPane;

/*
 * @Author Vitor Barbarulo
 */

public class ConversorTemp {

	public static void main(String[] args) {

		// entrada

		// CALCULAR NOVAMENTE
		int novamente;

		/*
		 * unidade: unidade de medida temp: temperatura em String
		 */
		String unidade, temp;

		double celsius, kelvin, fahrenheit;

		/*
		 * temperatura: temperatura convertida de String para double
		 */
		double temperatura;

		// EXECU��O DO PROGRAMA \/

		do {

			/*
			 * solicita ao usuario q informe a unidade de medida
			 */
			unidade = JOptionPane.showInputDialog("Informe a unidade de medida: ");

			if (unidade == null) {
				break;
			}
			// solicita que o user digite a temperatura
			temp = JOptionPane.showInputDialog("Informe a temperatura");

			// System.out.println("Unidade: " + unidade);
			// System.out.println("Temp: " + temp);

			temperatura = Double.parseDouble(temp);

			String msg;

			// processamento
			switch (unidade) {
			case "C":
			case "c":
			case "Celsius":
			case "CELSIUS":
			case "celsius":
				// converte Fahrenheit
				fahrenheit = (temperatura * 1.8) + 32;

				// pra kelvin
				kelvin = temperatura + 273.15;

				// msg de saida
				msg = fahrenheit + " �F\n" + kelvin + " K";
				JOptionPane.showMessageDialog(null, msg);
				break;

			case "F":
			case "f":
			case "Fahrenheit":
			case "FAHRENHEIT":
			case "fahrenheit":
				// convert to celsius
				celsius = (temperatura - 32) / 1.8;

				// convert to kelvin
				kelvin = (temperatura + 459.67) * 0.55555556;

				msg = celsius + " �C\n" + kelvin + " K";
				JOptionPane.showMessageDialog(null, msg);
				break;

			case "K":
			case "k":
			case "kelvin":
			case "Kelvin":
			case "KELVIN":
				// converte to celsius
				celsius = temperatura - 273.15;

				// convert to fahrenheit
				fahrenheit = temperatura * 1.8 - 459.67;

				msg = celsius + " �C\n" + fahrenheit + " �F\n";
				JOptionPane.showMessageDialog(null, msg);
				break;

			default:
				System.out.println("Unidade invalida");
				break;
			}// fim swith...case

			// ver se quer calcular novamente

			// 0 - sim
			// 1 - nao
			// 2 - cancelar

			novamente = JOptionPane.showConfirmDialog(null, "Deseja calcular novamente?", "Pergunta...", JOptionPane.YES_NO_OPTION);

		} while (novamente == 0);

	}// fim do main

}// fim class